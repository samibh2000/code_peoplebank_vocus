﻿using Pyramid.Lib.Models;
using Pyramid.Lib.Models.Enums;
using System;
using System.Collections.Generic;
using Pyramid.Lib.Helpers;

namespace Pyramid.Lib.BuisnessLogic
{
    public class PyramidBO
    {
        private List<PyramidNode> leaves = new List<PyramidNode>();
        private int highestTotal;

        private int LastCompleteLevel(int numberOfItems)
        {
            if (numberOfItems < 1)
            {
                throw new InvalidOperationException();
            }

            int level = 1;
            int items = level;
            while (numberOfItems > (items + level + 1))
            {
                level += 1;
                items += level;
            }
            return level;
        }

        private int TotalNodes(int level)
        {
            if (level < 1)
            {
                throw new InvalidOperationException();
            }
            int total = 0;
            for (int i = 1; i <= level; i++)
            {
                total += i;
            }
            return total;
        }

        private PyramidNode ReturnAvailableNode(
            EnumDefinitions.Side side, PyramidNode root, int finalLevel)
        {
            leaves.Clear();
            FindLeaves(root, 1, finalLevel, side);
            if (leaves.Count == 0)
            {
                throw new InvalidOperationException();
            };
            return leaves[0];
        }

        private void FindLeaves(
            PyramidNode node, int level, int finalLevel, EnumDefinitions.Side side)
        {
            if (level == finalLevel)
            {
                if (node.Left == null && side == EnumDefinitions.Side.Left)
                {
                    leaves.Add(node);
                }
                else
                {
                    if (node.Right == null && side == EnumDefinitions.Side.Right)
                    {
                        leaves.Add(node);
                    }
                }
                return;
            }
            if (node.Left != null)
            {
                FindLeaves(node.Left, level + 1, finalLevel, side);
            }
            if (node.Right != null)
            {
                FindLeaves(node.Right, level + 1, finalLevel, side);
            }
        }

        private void FindHighestTotal(PyramidNode node, int total)
        {
            if (node.Left == null && node.Right == null)
            {
                if (node.Value + total > highestTotal)
                {
                    highestTotal = node.Value + total;
                }
                return;
            }
            if (node.Left != null)
            {
                FindHighestTotal(node.Left, total + node.Value);
            }
            if (node.Right != null)
            {
                FindHighestTotal(node.Right, total + node.Value);
            }
        }

        public PyramidNode GeneratePyramid(int[][] data)
        {
            if (data.GetLength(0) == 0)
            {
                throw new ArgumentNullException();
            }

            PyramidNode root = new PyramidNode
            {
                Value = data[0][0],
            };
            int i = 1;
            while (i < data.GetLength(0))
            {
                if (data[i].Length <= data[i - 1].Length)
                {
                    throw new InvalidOperationException();
                }

                EnumDefinitions.Side side;
                int value;
                int index = data[i].SearchForSubset(data[i-1]);

                if (index < 0)
                {
                    throw new IndexOutOfRangeException();
                }
                if (index == 0)
                {
                    side = EnumDefinitions.Side.Right;
                    value = data[i][data[i].Length - 1];
                }
                else
                {
                    side = EnumDefinitions.Side.Left;
                    value = data[i][0];
                };
                int finalLevel = LastCompleteLevel(data[i].Length);
                int currentTotalNodes = TotalNodes(finalLevel);
                int targetTotalNodes = TotalNodes(finalLevel+1);
                PyramidNode available = ReturnAvailableNode(side, root, finalLevel);
                PyramidNode newNode = new PyramidNode
                {
                    Value = value
                };
                if (side == EnumDefinitions.Side.Left)
                {
                    available.AddLeft(newNode);
                    if (data[i].Length > currentTotalNodes + 1 && 
                        data[i].Length < targetTotalNodes)
                    {
                        PyramidNode otherAvailable = 
                            ReturnAvailableNode(EnumDefinitions.Side.Right, root, finalLevel);
                         otherAvailable.AddRight(newNode);
                    }
                }
                else
                {
                    available.AddRight(newNode);
                    if (data[i].Length > currentTotalNodes + 1 &&
                        data[i].Length < targetTotalNodes)
                    {
                        PyramidNode otherAvailable =
                            ReturnAvailableNode(EnumDefinitions.Side.Left, root, finalLevel);
                        otherAvailable.AddLeft(newNode);
                    }
                }
                i++;
            }
            return root;
        }

        public int HighestTotal(PyramidNode root)
        {
            highestTotal = 0;
            FindHighestTotal(root, 0);
            return highestTotal;
        }
    }
}
