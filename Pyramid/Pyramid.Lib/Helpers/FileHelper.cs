﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pyramid.Lib.Helpers
{
    public class FileHelper
    {
        public int[][] ReadFile(string fileName)
        {
            string line;
            char separator = ' ';
            List<int[]> tempList = new List<int[]>();

            if (File.Exists(fileName))
            {
                StreamReader file = null;
                try
                {
                    file = new StreamReader(fileName);
                    while ((line = file.ReadLine()) != null)
                    {
                        var intArray = Array.ConvertAll
                            (line.Split(separator), s => int.Parse(s));
                        tempList.Add(intArray);
                    }
                    var resultArray = tempList.ToArray();
                    return resultArray;
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    if (file != null)
                        file.Close();
                }
            }
            else
            {
                throw new FileNotFoundException();
            }
        }
    }
}
