﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pyramid.Lib.Helpers
{
    public static class Extension
    {
        public static int SearchForSubset(this int[] superset, int[] needle)
        {
            var len = needle.Length;
            var limit = superset.Length - len;
            for (var i = 0; i <= limit; i++)
            {
                var k = 0;
                for (; k < len; k++)
                {
                    if (needle[k] != superset[i + k])
                    {
                        break;
                    }
                }
                if (k == len)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}