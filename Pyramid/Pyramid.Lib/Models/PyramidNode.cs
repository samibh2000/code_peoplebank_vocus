﻿namespace Pyramid.Lib.Models
{
    public class PyramidNode
    {
        private PyramidNode left = null;
        private PyramidNode right = null;

        public int Value { get; set; }
        public PyramidNode Left
        {
            get { return left; }
        }
        public PyramidNode Right
        {
            get { return right; }
        }

        public void AddLeft(PyramidNode child)
        {
            this.left = child;
        }

        public void AddRight(PyramidNode child)
        {
            this.right = child;
        }
    }
}
