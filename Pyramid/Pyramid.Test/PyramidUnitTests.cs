﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pyramid.Lib.Helpers;
using Pyramid.Lib.BuisnessLogic;

namespace Pyramid.Test
{
    [TestClass]
    public class PyramidUnitTests
    {
        [TestMethod]
        public void GeneratePyramid_ProvideValidData_ShouldReturnRootNode()
        {
            // Arrange
            int[][] nodes = new int[10][];
            nodes[0] = new int[] { 26 };
            nodes[1] = new int[] { 26, 8 };
            nodes[2] = new int[] { 16, 26, 8 };
            nodes[3] = new int[] { 21, 16, 26, 8 };
            nodes[4] = new int[] { 21, 16, 26, 8, 41 };
            nodes[5] = new int[] { 21, 16, 26, 8, 41, 4 };
            nodes[6] = new int[] { 7, 21, 16, 26, 8, 41, 4 };
            nodes[7] = new int[] { 7, 21, 16, 26, 8, 41, 4, 1 };
            nodes[8] = new int[] { 12, 7, 21, 16, 26, 8, 41, 4, 1 };
            nodes[9] = new int[] { 12, 7, 21, 16, 26, 8, 41, 4, 1, 33 };
            PyramidBO helper = new PyramidBO();
            // Act
            var root = helper.GeneratePyramid(nodes);
            // Assert
            Assert.IsNotNull(root);
            Assert.AreEqual(26, root.Value);
            Assert.AreEqual(16, root.Left.Value);
            Assert.AreEqual(21, root.Left.Left.Value);
            Assert.AreEqual(7, root.Left.Left.Left.Value);
            Assert.AreEqual(16, root.Left.Value);
            Assert.AreEqual(41, root.Left.Right.Value);
            Assert.AreEqual(1, root.Left.Right.Left.Value);
            Assert.AreEqual(8, root.Right.Value);
            Assert.AreEqual(4, root.Right.Right.Value);
            Assert.AreEqual(33, root.Right.Right.Right.Value);
            Assert.AreEqual(8, root.Right.Value);
            Assert.AreEqual(4, root.Right.Right.Value);
            Assert.AreEqual(12, root.Right.Right.Left.Value);
        }

        [TestMethod]
        public void HighestTotal_ProvideValidData_ShouldReturnHighestTotal()
        {
            // Arrange
            int[][] nodes = new int[10][];
            nodes[0] = new int[] { 26 };
            nodes[1] = new int[] { 26, 8 };
            nodes[2] = new int[] { 16, 26, 8 };
            nodes[3] = new int[] { 21, 16, 26, 8 };
            nodes[4] = new int[] { 21, 16, 26, 8, 41 };
            nodes[5] = new int[] { 21, 16, 26, 8, 41, 4 };
            nodes[6] = new int[] { 7, 21, 16, 26, 8, 41, 4 };
            nodes[7] = new int[] { 7, 21, 16, 26, 8, 41, 4, 1 };
            nodes[8] = new int[] { 12, 7, 21, 16, 26, 8, 41, 4, 1 };
            nodes[9] = new int[] { 12, 7, 21, 16, 26, 8, 41, 4, 1, 33 };
            PyramidBO pyramidBO = new PyramidBO();
            var root = pyramidBO.GeneratePyramid(nodes);
            // Act
            var result = pyramidBO.HighestTotal(root);
            // Assert
            Assert.AreEqual(95, result);
        }

        [TestMethod]
        public void ReadPyramidData_ProvideValidData_ShouldReturnPyramidArrayStructure()
        {
            // Arrange
            FileHelper fileHelper = new FileHelper();
            // Act
            var nodes = fileHelper.ReadFile(@"..\..\..\Pyramid.Lib\Data\Data.txt");
            // Assert
            Assert.AreEqual(10, nodes.GetLength(0));
            Assert.AreEqual(26, nodes[0][0]);
            Assert.AreEqual(26, nodes[1][0]);
            Assert.AreEqual(8, nodes[1][1]);
            Assert.AreEqual(16, nodes[2][0]);
            Assert.AreEqual(26, nodes[2][1]);
            Assert.AreEqual(8, nodes[2][2]);
        }

        [TestMethod]
        public void AllToGether_ProvideValidData_ShouldReturnHighestTotal()
        {
            // Arrange
            PyramidBO pyramidBO = new PyramidBO();
            FileHelper fileHelper = new FileHelper();
            var nodes = fileHelper.ReadFile(@"..\..\..\Pyramid.Lib\Data\Data.txt");
            var root = pyramidBO.GeneratePyramid(nodes);
            // Act
            var result = pyramidBO.HighestTotal(root);
            // Assert
            Assert.AreEqual(95, result);
        }
    }
}
